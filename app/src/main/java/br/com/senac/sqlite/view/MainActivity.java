package br.com.senac.sqlite.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.sqlite.R;
import br.com.senac.sqlite.dao.AlunoDAO;
import br.com.senac.sqlite.model.Aluno;


public class MainActivity extends AppCompatActivity {

    private ListView listViewAlunos ;
    private ArrayAdapter<Aluno> adapter ;
    private  List<Aluno> lista;
    private AlunoDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    protected  void onResume(){
        super.onResume();

        listViewAlunos = findViewById(R.id.listViewAlunos);
        dao = new AlunoDAO(this);
        lista = dao.getLista();
        dao.close();
        adapter = new ArrayAdapter<Aluno>(this , android.R.layout.simple_list_item_1, lista);
        listViewAlunos.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu , menu);

        return true ;
    }

    public void adicionar(MenuItem item){

        Intent intent = new Intent(this , CadastroActivity.class);
        startActivity(intent);

    }
}
